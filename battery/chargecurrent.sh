#!/bin/sh

dir=/sys/class/power_supply/battery
# could also be /sys/class/power_supply/axp20x-battery
file1=constant_charge_current_max
file2=input_current_limited
# maximum current in mA from file, or default value:
currmax=1000

# this file is sourced and can override any of the vars above
conf=/home/defaultuser/.config/chargecurrent.env
[ -r $conf ] && . $conf

[ "$currmax" -gt 0 ] && [ "$currmax" -le 2400 ] || exit 1
cd "$dir" || exit 1

printf 'Current value %s:\t' $file1
cat $file1 || exit 1
printf 'Current value %s:    \t' $file2
cat $file2 || exit 1
chmod +w $file1 $file2 || exit 1

echo $(($currmax * 1000)) > $file1 && echo 1 > $file2

printf '    New value %s:\t' $file1
cat $file1
printf '    New value %s:    \t' $file2
cat $file2
