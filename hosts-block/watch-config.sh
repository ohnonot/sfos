#!/bin/ash

type inotifywait >/dev/null || exit 1

path="/home/defaultuser/.config/hosts-block"

wait_for_file() {
    while [ ! -r "$1" ]; do inotifywait -e create "${1%/*}"; done
    echo "File $1 exists now"
}
watch_whitelist() {
    file="$path/whitelist.txt"
    while :; do
        wait_for_file "$file"
        inotifywait -e modify "$file"
        echo "Launching hosts-block"
        /usr/bin/hosts-block
    done
}
watch_blacklist() {
    file="$path/blacklist.txt"
    while :; do
        wait_for_file "$file"
        inotifywait -e modify "$file"
        echo "Launching hosts-block"
        /usr/bin/hosts-block
    done
}
watch_config() {
    file="$path/cfg.sh"
    while :; do
        wait_for_file "$file"
        inotifywait -e modify "$file"
        echo "Launching hosts-block, forced"
        /usr/bin/hosts-block force
    done
}

watch_whitelist &
watch_blacklist &
watch_config &

wait
