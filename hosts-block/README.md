Ad-blocking via /etc/hosts ([primer][ah])

The script collects, sanitizes and compiles blocklists from the internet into a hosts file that will block domains on your whole system.

By default only [Steven Black's default list][sb] is used, but you can add as many sources as you want to create the ultimate ad-blocking hosts file.
This happens through configuration in /home/defaultuser/.config/cfg.sh, along with a few other options.
See the example config in /usr/share/doc/hosts-block.

The script is run once a day through a systemd timer/service, but the interval for fetching data is configurable.

It is also possible to white/blacklist domains through /home/defaultuser/.config/{white,black}list.txt - the format is much like hosts itself
but without the numerical IP, so just the \[sub.]domain.tld, one per line.  

- Whitelist means these domains will **never** be blocked.
- Blacklist means these domains will **always** be blocked.

Please keep in mind that the www. subdomain has to be specified explicitely, if so desired.
In other words, for most domains you'll probably want 2 lines:

    domain.tld
    www.domain.tld

# Before installing the package

There is nothing to do if you haven't customized /etc/hosts. But if you want to preserve its content please move/copy it to /etc/hosts.head
before installing this package.
If /etc/hosts.head does not exist yet it will be created with sane default values, and from then on it
will be used as the top of /etc/hosts unaltered.

It is, however, not a good idea to put to-be-blocked domains in /etc/hosts.head. Please use the blacklist feature for that (if you think
these domains haven't been covered by one of the available blocklists already).

When you uninstall hosts-block /etc/hosts.head will become your regular hosts file again.

# After installation 

..the script runs immediately, then daily. But it will only fetch new sources from the internet for the configured interval (default: 7 days).

Only if you want stricter or less strict blocking, can you copy /usr/share/doc/hosts-block/cfg.sh.example to /home/defaultuser/.config/cfg.sh
and edit it to your liking. You can also black/whitelist sites by creating /home/defaultuser/.config/whitelist.txt and blacklist.txt resp. (see top of this Readme).

The script sends various graphical notifications when something important happens (good or bad).

Source  
[Framagit.org](https://framagit.org/ohnonot/sfos/)  
[Notabug.org](https://notabug.org/ohnonot/sfos/)

[sb]: https://raw.githubusercontent.com/StevenBlack/hosts/master/hosts
[ah]: https://nordvpn.com/blog/use-hosts-file-block-ads-malware/
