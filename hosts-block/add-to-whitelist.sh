#!/bin/ash

# massage URL and add it to ~/.config/hosts-block/whitelist.txt

for dep in mkdir grep sed; do
    type $dep >/dev/null || exit 1
done

e="/usr/share/hosts-block/hosts-block-env.sh"
[ -r "$e" ] && . "$e" || { echo "Could not source environment. Quit."; exit 1; }

url="$1"
url="${url#http://}"
url="${url#https://}"
url="${url%%/*}"
[ -z "$url" ] && exit_err "continue because URL is empty after massaging"

[ -d "$cfgdir" ] || mkdir -p "$cfgdir"
[ -r "$whitelist" ] && grep "^$(echo "$url" | sed 's/\./\\./g')$" "$whitelist" && { notify "$url is already in whitelist"; exit 0; }

echo "$url" >> "$whitelist"

exit 0
