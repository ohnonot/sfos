#!/bin/ash

# massage URL and add it to ~/.config/hosts-block/blacklist.txt

for dep in mkdir grep sed; do
    type $dep >/dev/null || exit 1
done

e="/usr/share/hosts-block/hosts-block-env.sh"
[ -r "$e" ] && . "$e" || { echo "Could not source environment. Quit."; exit 1; }

url="$1"
url="${url#http://}"
url="${url#https://}"
url="${url%%/*}"
[ -z "$url" ] && exit_err "continue because URL is empty after massaging"

[ -d "$cfgdir" ] || mkdir -p "$cfgdir"
[ -r "$blacklist" ] && grep "^$(echo "$url" | sed 's/\./\\./g')$" "$blacklist" && { notify "$url is already in blacklist"; exit 0; }

echo "$url" >> "$blacklist"

exit 0
