#### User configurable variables for hosts-block

## Default values are commented out with a single #

## This is where bad domains are redirected to.
## Some people prefer 127.0.0.1
##
prepend="0.0.0.0"

## Absolute minimum of unique entries; less than that
## (before removing/adding white/blacklisted entries)
## and the current hosts file will not be replaced
##
minlines=10000

## update interval in days, integer only
##
interval=7

## how many seconds to wait before giving up
## (currently only applies when another instance of hosts-block is running)
##
timeout=300

## This is the default: just one hosts list
## (Steven Black's basic list)
##
uassets=""
##
metalists=""
##
extraurls="https://raw.githubusercontent.com/StevenBlack/hosts/master/hosts"

######### Ultra-high protection example #############
##
## uncomment all variables for maximum blocking.
## Each one can contain multiple urls separated by
## newlines (like extraurls below)

## uassets Must be in tar.gz format. top folder must
## be named *u[aA]ssets* under which we look for
## thirdparties and filters subfolders
##
##uassets="https://github.com/uBlockOrigin/uAssets/archive/refs/heads/master.tar.gz"

## A list of urls containing blocklists. Only lines
## starting with http(s) are considered:
##
##metalists="https://v.firebog.net/hosts/lists.php?type=tick"

## Extra urls of blocklists, 1 per line:
## (note the quotes, the variable contains newlines)
##
##extraurls="https://malware-filter.gitlab.io/malware-filter/phishing-filter-hosts.txt
##https://malware-filter.gitlab.io/malware-filter/urlhaus-filter-hosts.txt
##https://raw.githubusercontent.com/StevenBlack/hosts/master/alternates/fakenews-gambling-porn-social/hosts"
##
#####################################################

## you probably do not need to change these
##
## The final destination
hosts="/etc/hosts"
hostshead="$hosts.head"
##
## For both temporary and persistent data
datadir="/var/lib/hosts-block"
##
## set to 1: keep temporary files in $datadir
debug=0
##
## set to 1, the script will fail as soon as one list
##           cannot be downloaded
## set to 0, the script will still fail if some
##           crucial steps do not succeed
bail=0

id=100000
user="$(getent passwd $id || echo defaultuser)"
user="${user%%:*}"
cfgdir="/home/$user/.config/hosts-block"
cfg="$cfgdir/cfg.sh"
# contains a list of entries that should be discarded
# format as in hosts file, i.e. [subdomain(s).]domain.tld
whitelist="$cfgdir/whitelist.txt"
# contains a list of entries that should be added
blacklist="$cfgdir/blacklist.txt"

# override above variables in a config file if desired
[ -r "$cfg" ] && . "$cfg" && echo "Sourced $cfg"
#########################

exit_err() {
    notify "Could not $@. Exiting."
    exit 1
}

notify() {
    echo "Notifying: $*"
    export DBUS_SESSION_BUS_ADDRESS=unix:path=/run/user/$id/dbus/user_bus_socket
    /usr/bin/notificationtool -A hosts-block -I hosts-block -o add "$@"
}
