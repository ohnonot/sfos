# History

I reworked my hosts-based adblock script based on these insights:

>The developer of [this tool][tt] also made [this one][to]. Not sure what the difference is, but it gives far less flattering results.
>
>Using [firebog.net][fb]’s meta list + Steven Black's largest list combined into one big hosts file I get ~80% with both tools.
>
>A slightly stricter µBlock configuration gives me 100% though, and µBlock gets a lot of its lists in AdBlock format. I will try to figure out how to best convert these to hosts format.

If I use a combination of different hosts-based domain lists (as above) I get 87% and 65% resp. with those two test tools.

If I convert and add domains from [ublock's lists][ua] to that I get 91% and 87% resp., even though this adds "only" a few thousand unique entries.

Therefore the script has grown quite complex:

- it converts uBO's [uAssets][ua] as far as possible to be used by /etc/hosts
- it downloads everything from firebog.net's meta list plus Steven Black's largest hosts file
- it combines and unique-sorts everything, adds a header and finally overwrites the old /etc/hosts

Ultimately it comes down to get a timely combination of as many sources as possible. I'm already re-inventing the wheel here; various more configurable solutions exist. But then, the whole thing is simple enough.

Keep in mind that uBO does more than domain blocking, and many adblock-specific rules can never be part of hosts-based blocking.  
OTOH hosts-based blocking is always system-wide **and** very light on resources.

# Android App Support

It is possible to use the same hosts file under AAS. Create `/usr/libexec/appsupport/alien-generate-extra-config.sh` with the following content, and make it executable:

~~~sh
#!/bin/sh

echo ">>> $0" >&2
echo ">>> \$1 $1" >&2
echo ">>> \$2 $2" >&2

[ "$#" -gt 1 ] && shift

echo "lxc.mount.entry = /etc/hosts system/etc/hosts none bind,create=file 0 0" > "$1/29-hosts_config"
~~~

Last tested in 2024.

[tt]: https://d3ward.github.io/toolz/adblock.html
[to]: https://test.adminforge.de/
[fb]: http://firebog.net
[ua]: https://github.com/uBlockOrigin/uAssets
