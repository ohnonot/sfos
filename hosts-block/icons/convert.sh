#!/bin/sh

[ $# -eq 1 ] || { echo "Please provide exactly one SVG file on the command line"; exit 1; }
[ -r "$1" ] || { echo "Cannot read $1"; exit 1; }

type rsvg-convert > /dev/null || exit 1

for s in 86 108 128 172; do
    dir="${s}x$s"
    mkdir -p "$dir" || continue
    out="$dir/${1%.*}.png"
    rsvg-convert --width=$s --height=$s --keep-aspect-ratio "$1" > "$out"
    type pngquant > /dev/null && pngquant \
    --nofs --speed 1 --skip-if-larger --strip --ext .png --force "$out"
done
