Name:               hosts-block
Version:            0.0.7
Release:            1
Summary:            Hosts-based adblock script
BuildArch:          noarch
License:            GPLv3
URL:                https://framagit.org/ohnonot/sfos
Source:             %{name}-%{version}.tar.gz
Requires:           lipstick-qt5-tools inotify-tools
BuildRequires:      pkgconfig(systemd)

%description
Hosts-based adblock script

%if 0%{?_chum}
Title:              Hosts-block
Custom:
  Repo:             https://framagit.org/ohnonot/sfos
  PackagingRepo:    https://framagit.org/ohnonot/sfos
  DescriptionMD:    https://framagit.org/ohnonot/sfos/-/raw/master/hosts-block/README.md
PackageIcon:        https://framagit.org/ohnonot/sfos/-/raw/master/hosts-block/icons/hosts-block.svg
Links:
  Help:             https://forum.sailfishos.org/t/ad-blocking-in-sailfishos-in-2023-what-are-the-options/15799/26
Categories:
  - System
DeveloperName:      Ohnonot
PackagedBy:         Vlad G., Ohnonot

%endif

%files
%{_bindir}/%{name}

%{_unitdir}/%{name}.service
%{_unitdir}/%{name}-watch-config.service
%{_unitdir}/%{name}.timer

%{_docdir}/%{name}/README.md
%{_docdir}/%{name}/cfg.sh

%{_datadir}/%{name}/%{name}-env.sh
%{_datadir}/%{name}/watch-config.sh
%{_datadir}/%{name}/add-to-whitelist.sh
%{_datadir}/%{name}/add-to-blacklist.sh

%{_datadir}/applications/%{name}-add-to-whitelist.desktop
%{_datadir}/applications/%{name}-add-to-blacklist.desktop

%{_datadir}/icons/hicolor/*/apps/%{name}.png

%prep
%setup -q

%build

%install
cd %{name}
%{__install} -D -m 755 %{name} %{buildroot}%{_bindir}/%{name}

%{__install} -D -m 644 README.md %{buildroot}%{_docdir}/%{name}/README.md
%{__install} -D -m 644 cfg.sh %{buildroot}%{_docdir}/%{name}/cfg.sh

%{__install} -D -m 644 %{name}-env.sh %{buildroot}%{_datadir}/%{name}/%{name}-env.sh
%{__install} -D -m 755 watch-config.sh %{buildroot}%{_datadir}/%{name}/watch-config.sh
%{__install} -D -m 755 add-to-whitelist.sh %{buildroot}%{_datadir}/%{name}/add-to-whitelist.sh
%{__install} -D -m 755 add-to-blacklist.sh %{buildroot}%{_datadir}/%{name}/add-to-blacklist.sh

%{__install} -D -m 644 %{name}-add-to-whitelist.desktop %{buildroot}%{_datadir}/applications/%{name}-add-to-whitelist.desktop
%{__install} -D -m 644 %{name}-add-to-blacklist.desktop %{buildroot}%{_datadir}/applications/%{name}-add-to-blacklist.desktop

for s in 86 108 128 172; do\
    dir="${s}x$s";\
    %{__install} -D -m 644 icons/$dir/%{name}.png %{buildroot}%{_datadir}/icons/hicolor/$dir/apps/%{name}.png;\
done

cd systemd
%{__install} -D -m 644 %{name}.service %{buildroot}%{_unitdir}/%{name}.service
%{__install} -D -m 644 %{name}-watch-config.service %{buildroot}%{_unitdir}/%{name}-watch-config.service
%{__install} -D -m 644 %{name}.timer %{buildroot}%{_unitdir}/%{name}.timer

%post
#%systemd_post %{name}.timer %{name}-watch-config.service
%systemd_post %{name}.timer
#%{_bindir}/systemctl start %{name}.timer %{name}.service %{name}-watch-config.service
%{_bindir}/systemctl start %{name}.timer %{name}.service
%{_bindir}/desktop-file-install --dir=%{_datadir}/applications %{_datadir}/applications/%{name}-add-to-blacklist.desktop
%{_bindir}/desktop-file-install --rebuild-mime-info-cache --dir=%{_datadir}/applications %{_datadir}/applications/%{name}-add-to-whitelist.desktop
%{_bindir}/update-desktop-database %{_datadir}/applications

%preun
%systemd_preun %{name}.timer %{name}-watch-config.service
%{_bindir}/test -r /etc/hosts.head && %{_bindir}/mv /etc/hosts.head /etc/hosts

%postun
%{_bindir}/rmdir /usr/share/hosts-block/ /usr/share/doc/hosts-block/
%{_bindir}/update-desktop-database %{_datadir}/applications

%changelog
* Mon Mar 10 2025 - Ohnonot <ohnonot@anche.no> - 0.0.7-0
- another major overhaul to fix enable reload on filechange 😳
* Tue Mar 04 2025 - Ohnonot <ohnonot@anche.no> - 0.0.6-0
- major overhaul to enable reload on filechange
* Sat Feb 22 2025 - Ohnonot <ohnonot@anche.no> - 0.0.5-0
- added icons and blacklist options
- now in chum:testing 🥳
* Sat Feb 15 2025 - Ohnonot <ohnonot@anche.no> - 0.0.4-1
- added configuration and whitelist options
* Thu Feb 13 2025 - Ohnonot <ohnonot@anche.no> - 0.0.3-1
- Script improvements
- Service & timer improvements
- Rework systemd commands in spec file to make sure the hosts file is updated immediately after installation
* Sun Jan 12 2025 - Vlad G. <vlad@grecescu.net> - 0.0.2-1
- Send notification to restart browser after update
* Wed Jan 08 2025 - Vlad G. <vlad@grecescu.net> - 0.0.1-2
- Initial rpm packaging
* Sun Oct 29 2023 - Ohnonot <ohnonot@anche.no> - 0.0.1
- Development and announcement on forum.sailfishos.org
